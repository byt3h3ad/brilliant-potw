from datetime import date
import calendar

year = 2018
c = calendar.TextCalendar(calendar.SUNDAY)
list = []
for m in range(1, 13):
	for i in c.itermonthdays(year, m):
		if i != 0:  #calendar constructs months with leading zeros (days belongng to the previous month)
			day = date(year, m, i)
			if day.weekday() == 0:
				#print("%s-%s-%s" % (year, m, i))
				#print(f"'{m}-{i}'", end = ", ")
				list.append(f"{m}-{i}")
print(list)
